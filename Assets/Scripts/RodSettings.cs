﻿using UnityEngine;

[System.Serializable]
public class RodSettings
{
    [SerializeField]
    [Range(0, 90)]
    private float upperAngle = 45;

    [SerializeField]
    [Range(0, -90)]
    private float lowerAngle = -45;

    [SerializeField]
    private float rotationSpeed = 200;

    public float MaxAngle { get { return upperAngle; } }
    public float MinAngle { get { return lowerAngle; } }
    public float RotationSpeed { get { return rotationSpeed; } }
}