﻿using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField]
    private FishingRod fishingRod;
    [SerializeField]
    private Rope rope;

    private void Update()
    {
        ProcessInput();
    }

    private void ProcessInput()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            fishingRod.SetMotorSpeed(fishingRod.RodSettings.RotationSpeed);
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            fishingRod.SetMotorSpeed(-fishingRod.RodSettings.RotationSpeed);
        }
        else
        {
            fishingRod.SetMotorSpeed(0);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rope.ChangeState(false);
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rope.ChangeState(true);
        }
    }
}
