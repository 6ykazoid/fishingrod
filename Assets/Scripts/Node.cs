﻿using UnityEngine;

public class Node : MonoBehaviour
{
    public Rigidbody2D Rigidbody { get; private set; }
    public DistanceJoint2D Joint { get; private set; }
    public bool IsConnected { get { return Joint.connectedBody != null; } }

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
        Joint = GetComponent<DistanceJoint2D>();
        Joint.enabled = false;
    }

    public void ConnectTo(Rigidbody2D rigidbody)
    {
        Joint.enabled = true;
        Joint.connectedBody = rigidbody;
    }

    public void Disconnect()
    {
        Joint.connectedBody = null;
        Joint.enabled = false;
    }
}
