﻿using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
    [SerializeField]
    private GameObject weightPrefab;
    [SerializeField]
    private Node ropeNodePrefab;
    [SerializeField]
    private Rigidbody2D connectionPoint;
    [SerializeField]
    private RopeSettings ropeSettings;

    private int minNodes;
    private int maxNodes;
    private Stack<Node> ropeNodes;
    private bool needToRelease;
    private bool prevState;

    private float tailDestroyRadius = 0.2f;
    private float nodeAddThreshold = 0.7f;

    private Vector2 TailRelativePos { get { return (Vector2)connectionPoint.transform.position - Tail.Rigidbody.position; } }

    private Node Tail
    {
        get
        {
            if (ropeNodes != null && ropeNodes.Count > 0)
            {
                return ropeNodes.Peek();
            }
            return null;
        }
    }

    public Node[] Nodes { get { return ropeNodes.ToArray(); } }
    public Vector3 ConnectionPointPosition { get { return connectionPoint.transform.position; } }

    private void Awake()
    {
        minNodes = Mathf.RoundToInt(ropeSettings.StartLength / ropeSettings.SegmentLength);
        maxNodes = Mathf.RoundToInt(ropeSettings.MaxLength / ropeSettings.SegmentLength);
        ropeNodes = new Stack<Node>(maxNodes);

        InitializeRope();
    }

    private void InitializeRope()
    {
        float startLength = minNodes * ropeSettings.SegmentLength;

        Vector2 weightPosition = connectionPoint.transform.position + Vector3.down * startLength;
        GameObject weight = Instantiate(weightPrefab, weightPosition, Quaternion.identity, this.transform);
        AddNode(weightPosition + Vector2.up * ropeSettings.SegmentLength);
        weight.GetComponent<Joint2D>().connectedBody = Tail.Rigidbody;

        RegenerateRope();

        needToRelease = true;
    }

    private void AddNode(Vector2 pos)
    {
        Node node = Instantiate(ropeNodePrefab, pos, Quaternion.identity, this.transform);
        if (ropeNodes.Count > 0)
        {
            Tail.ConnectTo(node.Rigidbody);
        }
        ropeNodes.Push(node);
    }

    private void RegenerateRope()
    {
        while (ropeNodes.Count < maxNodes && TailRelativePos.magnitude > ropeSettings.SegmentLength * nodeAddThreshold)
        {
            AddNode(Tail.Rigidbody.position + TailRelativePos.normalized * ropeSettings.SegmentLength);
        }
    }

    private void Update()
    {
        if (needToRelease && ropeNodes.Count < maxNodes)
        {
            ReleaseRope();
        }
        else if (!needToRelease && ropeNodes.Count > minNodes)
        {
            RetractRope();
        }
        else
        {
            StopRope();
        }
    }

    private void StopRope()
    {
        if (!Tail.IsConnected)
            Tail.ConnectTo(connectionPoint);
    }

    private void RetractRope()
    {
        if (Tail.IsConnected)
            Tail.Disconnect();

        Tail.Rigidbody.MovePosition(connectionPoint.transform.position);

        if (TailRelativePos.magnitude <= tailDestroyRadius)
            Destroy(ropeNodes.Pop().gameObject);
        if (ropeNodes.Count <= minNodes)
            StopRope();
    }

    private void ReleaseRope()
    {
        RegenerateRope();
        if (ropeNodes.Count >= maxNodes)
            StopRope();
    }

    public void ChangeState(bool value)
    {
        needToRelease = value;
    }
}
