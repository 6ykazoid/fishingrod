﻿using System;
using UnityEngine;

public class FishingRod : MonoBehaviour
{
    [SerializeField]
    private RodSettings rodSettings;

    private Rigidbody2D rb;
    private HingeJoint2D joint;
    private JointMotor2D motor;

    public RodSettings RodSettings { get { return rodSettings; } }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        joint = GetComponent<HingeJoint2D>();
        joint.limits = new JointAngleLimits2D() { max = rodSettings.MaxAngle, min = rodSettings.MinAngle };
        motor = joint.motor;
    }

    public void SetMotorSpeed(float speed)
    {
        motor.motorSpeed = speed;
        joint.motor = motor;
    }
}
