﻿using UnityEngine;

[System.Serializable]
public class RopeSettings
{
    [SerializeField]
    private float startLength = 5;
    [SerializeField]
    private float maxLength = 10;
    [SerializeField]
    private float segmentLength = 0.3f;
    
    public float StartLength { get { return startLength; } }
    public float MaxLength { get { return maxLength; } }
    public float SegmentLength { get { return segmentLength; } }
}