﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RopeRenderer : MonoBehaviour
{
    private Rope rope;
    private LineRenderer lr;

    private void Awake()
    {
        rope = GetComponent<Rope>();
        lr = GetComponent<LineRenderer>();
    }

    private void LateUpdate()
    {
        RenderLine();
    }

    private void RenderLine()
    {
        Node[] nodes = rope.Nodes;
        Vector3[] points = new Vector3[nodes.Length + 1];
        points[0] = rope.ConnectionPointPosition;
        for (int i = 0; i < nodes.Length; i++)
        {
            points[i+1] = nodes[i].transform.position;
        }
        lr.positionCount = points.Length;
        lr.SetPositions(points);
    }
}
